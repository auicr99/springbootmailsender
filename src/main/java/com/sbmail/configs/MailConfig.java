package com.sbmail.configs;

import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.sbmail.constants.MailConstant;

@Configuration
public class MailConfig {
    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        
        // Sử dụng host SMTP để gửi mail không bị giới hạn
        mailSender.setHost("smtp.gmail.com");
        // Set port - cổng cho SMTP - mặc định là 578
        mailSender.setPort(587);
        
        // Config mail và password của sender
        mailSender.setUsername(MailConstant.SENDER_EMAIL);
        mailSender.setPassword(MailConstant.SENDER_EMAIL_PASSWORD);
 
        // Config các properties cho JavaMail
        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");
 
        return mailSender;
    }
}

