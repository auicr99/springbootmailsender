package com.sbmail.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sbmail.constants.MailConstant;

@Controller
public class SimpleMailController {
	@Autowired
	JavaMailSender javaMailSender;
	
	@RequestMapping("/sendSimpleEmail")
	@ResponseBody
	public String sendSimpleEmail() {
		// Tạo instance -> tạo một message đơn giản
		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
		
		// Set mail người nhận
		simpleMailMessage.setTo(MailConstant.RECEIVER_EMAIL);
		
		// Set Subject Title cho email
		simpleMailMessage.setSubject("Simple Email Testing");
		
		// Set content cho email
		simpleMailMessage.setText("Hi, I'm testing my Simple Email");
		
		// Ngoài ra còn có thể cc/bcc người nhận qua hàm setCc(), setBcc()
		
		javaMailSender.send(simpleMailMessage);
		return "Email sent!";
	}
}


