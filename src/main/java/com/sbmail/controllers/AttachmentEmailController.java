package com.sbmail.controllers;

import java.io.File;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sbmail.constants.MailConstant;

@Controller
public class AttachmentEmailController {
    @Autowired
    JavaMailSender javaMailSender;
    
    @RequestMapping("/sendAttachmentMail")
    @ResponseBody
    public String sendAttachmentMail() throws MessagingException {
    	// Tạo instance của MimeMessage -> cho phép gửi mail có các tập tin đính kèm
    	MimeMessage mimeMessage = javaMailSender.createMimeMessage();
  
    	boolean multipart = true;
    	
    	// Trợ giúp để tạo ra một tin nhắn MIME, nó hỗ trợ image, và các tập tin đính kèm
    	MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, multipart);
    	
    	// Set mail người nhận
    	mimeMessageHelper.setTo(MailConstant.RECEIVER_EMAIL);
    	
    	// Set Subject Title cho email
    	mimeMessageHelper.setSubject("Attachment Email Testing");
    	
    	// Set content cho email
    	mimeMessageHelper.setText("Hi, I'm testing my attchment email!");
    	
    	// Declare đường dẫn cho File cần attach
    	String textPath = "D:/Test/hello.txt";
        String filePath = "D:/Test/dnt_logo.png";
        
        // Tạo và add vào mimeMessageHelper
        FileSystemResource file1 = new FileSystemResource(new File(textPath));
        mimeMessageHelper.addAttachment("Txt", file1);
        FileSystemResource file2 = new FileSystemResource(new File(filePath));
        mimeMessageHelper.addAttachment("Image", file2);
        
        javaMailSender.send(mimeMessage);
        
    	return "Email sent!";
    }
}