package com.sbmail.controllers;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sbmail.constants.MailConstant;

@Controller
public class HtmlEmailController {
    @Autowired
    JavaMailSender javaMailSender;
    
    @RequestMapping("/sendHmtlMail")
    @ResponseBody
    public String sendHmtlMail() throws MessagingException {
    	// Tạo instance của MimeMessage -> cho phép gửi mail có các tập tin đính kèm hoặc mail định dạng HTML
    	MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        boolean multipart = true;
        
        // Trợ giúp để tạo ra một tin nhắn MIME, nó hỗ trợ image, và các tập tin đính kèm hoặc mail định dạng HTML
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, multipart, "utf-8");
        
        // Tạo htmlMessage bằng các html tags
        String htmlMessage = "<h3>Hi, I'm testing my HTML email</h3>"
                +"<img src='https://i.pinimg.com/originals/65/7c/bb/657cbb0ebdfb3d99ff3f446da5ea520b.jpg'>";
        
        // Set content cho mail với định dạng "text/html"
        mimeMessage.setContent(htmlMessage, "text/html"); 
        
        // Set mail người nhận
        mimeMessageHelper.setTo(MailConstant.RECEIVER_EMAIL); 
        
        // Set Subject title của mail
        mimeMessageHelper.setSubject("HTML Email Testing");   
        
        javaMailSender.send(mimeMessage);
    	return "Email sent!";
    }
}
